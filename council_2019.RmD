---
title: "Council"
author: "James Green"
date: "12/10/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set
library(tidyverse)

council2019 <- read_tsv("council.txt")

ggplot(council2019, aes(Iteration, Vote, colour = Surname)) +
  geom_line()

ggsave("council2019.png", width = 20, height = 10, units = "cm")

council2 <- filter(council2019, Iteration > 40 & Vote > 0)

ggplot(council2, aes(Iteration, Vote, colour = Surname)) +
  geom_line() +
  scale_y_continuous(limits = c(1000, 3000)) +
  theme_bw(14)

ggsave("council2019 v2.png", width = 20, height = 15, units = "cm")
```
